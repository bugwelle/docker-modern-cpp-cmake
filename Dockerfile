FROM ubuntu:latest

RUN apt-get update && \
    apt-get install -y software-properties-common && \
    add-apt-repository ppa:ubuntu-toolchain-r/test

RUN apt-get update && \
    apt-get install -y zlib1g zlib1g-dev g++-11 gcc-11 git wget curl \
        libclang-12-dev clang-tidy-12 clang-tools-12 clang-format-12 clang-12 \
        python3 python3-yaml python3-pip libjson-perl \
        libncurses5-dev libncurses5 ninja-build doxygen && \
    update-alternatives --install /usr/bin/gcc           gcc          /usr/bin/gcc-11          10 && \
    update-alternatives --install /usr/bin/gcov          gcov         /usr/bin/gcov-11         10 && \
    update-alternatives --install /usr/bin/g++           g++          /usr/bin/g++-11          10 && \
    update-alternatives --install /usr/bin/clang++       clang++      /usr/bin/clang++-12      10 && \
    update-alternatives --install /usr/bin/clang         clang        /usr/bin/clang-12        10 && \
    update-alternatives --install /usr/bin/clang-format  clang-format /usr/bin/clang-format-12 10 && \
    update-alternatives --install /usr/bin/clang-tidy    clang-tidy   /usr/bin/clang-tidy-12   10

COPY install_cppcheck.sh /opt/install_cppcheck.sh

RUN pip3 install cmake cmake_format conan meson
RUN /opt/install_cppcheck.sh

RUN cd /opt && git clone https://github.com/linux-test-project/lcov.git && cd lcov && make install
RUN perl -MCPAN -e 'install PerlIO::gzip'
RUN perl -MCPAN -e 'JSON'
