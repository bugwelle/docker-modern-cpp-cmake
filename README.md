# docker-modern-cpp-cmake

Docker image with:

 - GCC 11, Clang 12
 - Latest CMake fromo pip
 - cppcheck
 - include-what-you-use
 - cmake-format
 - clang-format
 - clang-tidy
 - lcov

It is used for some of my private projects.
